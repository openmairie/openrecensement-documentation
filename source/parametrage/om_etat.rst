.. _etat:


Il est proposé de décrire dans ce chapitre le paramétrage des états

Le sétats par défaut sont :

- notice

- recepisse

- avis

- attestation


(voir inscription)



Le paramétrage des états
=============================

La liste des états  est accessible dans l option du menu administration -> etat

.. image:: ../_static/tab_etat.png

Le paramétrage des états se fait dans le formulaire suivant

.. image:: ../_static/form_etat.png

La description du paramétrage est faite dans le guide du développeur.

