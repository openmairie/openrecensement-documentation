.. _ville:

Il est proposé de décrire dans ce chapitre le paramétrage des ville

la base pays est fourni dans le script sql : initville1.sql à initville7.sql

Les données sont partagés en plusieurs fichiers pour ne pas exceder la taille limite de phpmyadmin



Le paramétrage des villes
=========================

La liste des pays est dans parametrage option pays

.. image:: ../_static/tab_ville.png

Le paramétrage des evenements se fait dans le formulaire pays

.. image:: ../_static/form_ville.png
