.. _pays:


Il est proposé de décrire dans ce chapitre le paramétrage des utilisateurs

Par défaut, il est créé 2 utilisateurs :

Login : admin
pwd : admin

login : demo
pwd : demo


Ce login et pwd sont nécessaires pour accéder à l'application :

.. image:: ../_static/acces.png


Le paramétrage des profils
==========================

La liste des profils est paramétrable dans administration -> profil

.. image:: ../_static/tab_profil.png

La saisie se fait dans la grille suivante

.. image:: ../_static/form_profil.png


Les profils créés sont ensuite affectés à des droits et à des utilisateurs

le paramétrage des droits
=========================

La liste des droits est paramétrable dans administration -> droit

.. image:: ../_static/tab_droit.png

La saisie se fait dans la grille suivante

.. image:: ../_static/form_droit.png


Le paramétrage des utilisateurs
===============================

La liste des profils est utilisateurs dans administration -> utilisateurs

.. image:: ../_static/tab_utilisateur.png

Le paramétrage des utilisateurs se fait dans le formulaire suivant

.. image:: ../_static/form_utilisateur.png

