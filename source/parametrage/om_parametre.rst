.. _om_parametre:


Il est proposé de décrire dans ce chapitre le paramétrage général de la collectivité



La collectivite
===============

.. image:: ../_static/tab_collectivite.png

Le paramétrage des evenements se fait dans le formulaire pays

.. image:: ../_static/form_collectivite.png



Le paramétrage de la collectivité
=================================


Le paramétrage par défaut des options se fait dans om_collectivité :


La liste des pays est dans parametrage option pays

.. image:: ../_static/tab_parametre.png

Le paramétrage des evenements se fait dans le formulaire pays

.. image:: ../_static/form_parametre.png

