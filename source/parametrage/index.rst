.. _parametrage:

###########
Paramétrage
###########


Nous vous proposons dans ce chapitre de parametrer openFoncier :

- le paramétrage de la collectivité

- le paramétrage des utilisateurs et l'accès à l'application

- le paramétrage des états 

- la saisie des pays

- la saisie des villes



.. toctree::

    om_parametre.rst
    om_utilisateur.rst
    om_etat.rst
    pays.rst
    ville.rst
    
    
