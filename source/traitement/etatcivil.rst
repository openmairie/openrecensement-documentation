.. _etatcivil:

#####################################
Récuperer les données de l'état civil
#####################################


L'objectif est  de récupérer les donnees etat civil dans la table inscription_temp :


Export des données de l'état civil
    
    app/digitech.sql : requete dans la base oracle etat civil de digitech par date de naissance
    
Import en table inscription_temp
    
    scr/import.php : import en table inscription_temp



=================================
Récupérer les données de DIGITECH
=================================

Lancer la requete sql de récupération de données qui est dans le repertoire app/digitech.sql.

et importer le fichier csv de sortie.


=======================
integrer le fichier csv
=======================

administration -> import

table : inscription_temp

Voir guide l utilisateur openMairie pour plus d information sur le script d'import.

