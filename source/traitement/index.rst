.. _traitement:


##########
Traitement
##########

Nous vous proposons dans ce chapitre de décrire les traitements openpersonnalite :

- transfert des données via "pecoto"

- importer les données de l'état civil : cas de digitech

- l'archivage




.. toctree::

   pecoto.rst
   etatcivil.rst
   archivage.rst
