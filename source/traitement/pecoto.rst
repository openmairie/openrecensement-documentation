.. _pecoto:


################
transfert pecoto
################

Ce traitement permet de transferer les données au service national

Il faut au préalable paramétrer dans administration -> om_parametre

- insee

- cp

- ville

- nom_recensement

- tel_recensement

- mail_recensement

Pour lancer le traitement : traitement -> pecoto

Chosir un trimestre

.. image:: ../_static/pecoto.png

Cliquer sur le lien pour récupérer le fichier à envoyer

.. image:: ../_static/pecoto2.png


Le transfert se fait chaque trimestre.


<note>

    Le traitement reprend  des fonctions et descriptif depuis le logiciel libre BISCOTO
    
    http://adullact.net/projects/biscoto/
    
    Merci à
    
    y.effinger@ville-bischheim.fr

</note>