.. _inscription_temp:

######################
Inscription temporaire
######################


inscription -> inscription état civil


Cette écran reprend les jeunes ayant 16 ans dans l'année depuis le fichier d'état civil)
(voir chapître traitement)

Un bouton sur chaque ligne permet de créer un enregistrement correspondant dans inscription en reprenant
les données de l'état civil



.. image:: ../_static/tab_inscription_temp.png

