.. _inscription:




######################
saisir une inscription
######################



Il est proposé de décrire dans ce paragraphe la saisie d'inscription pour le recensement



Il est possible de voir la liste des inscriptions dans la grille ci dessous


.. image:: ../_static/tab_inscription.png


Il est possible de creer ou modifier une inscription dans le formulaire ci dessous


.. image:: ../_static/form_inscription.png

Saisir les zones nécessaires à l'inscription.



Les etats d'inscription :


Notice dinscription :

.. image:: ../_static/notice.png

.. image:: ../_static/notice2.png


Attetation à remettre à l'inscription :

.. image:: ../_static/attestation.png

Avis :

.. image:: ../_static/avis.png

Recepissé :

.. image:: ../_static/recepisse.png