.. openrecensement documentation master file


=================================
openRecensement 2.0 Documentation
=================================


.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


openRecensement est un outil au service de la collectivité locale (commune) pour effectuer le
recensement des jeunes qui ont 16 ans dans l'année en cours.
Ce resencement est envoyé chaque trimestre au service national régional afin de gérer l'appel
à la journée militaire nationale. http://www.openmairie.org/catalogue/openrecensement

Nous vous poposons dans cette documentation :

- un premier chapître sur l'utilisation courante : saisie des inscriptions

- un second chapître consacré au paramètrage : pays, ville, modification d' état, gestion des utilisateurs

- un troisième chapître concerne les traitements : le traitement PECOTO , la récupération de données de l'état civil (cas de digitech) et l'archivage.

- un quatrième chapître est consacré à l'intégration.



Ce document a pour but de guider les utilisateurs et les développeurs dans la prise en main du projet. Bonne lecture et n'hésitez pas à venir discuter du projet avec la communauté à l’adresse suivante : https://communaute.openmairie.org/c/autres-applications-openmairie/openrecensement

   
utilisation
===========

.. toctree::
   :maxdepth: 3

   utilisation/index.rst

parametrage
===========

.. toctree::
   :maxdepth: 3

   parametrage/index.rst


traitement
==========

.. toctree::
   :maxdepth: 3

   traitement/index.rst

integration
===========

.. toctree::
   :maxdepth: 3

   integration/index.rst



Bibliographie
=============

* http://www.openmairie.org/


Contributeurs
=============

(par ordre alphabétique)

* Jean Louis BASTIDE
* francois RAYNAUD
