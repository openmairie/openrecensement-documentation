.. _widget:

###########
les widgets
###########


openrecensement permet de construire une application composite en integrant 
des widgets dans le tableau de bord.

Le mode de fonctionnement des widget est décrit dans le guide du développeur openMairie.

Il est decrit ensuite l'integration au travers de tableau de bord personnalisés.


.. image:: ../_static/tdb.png

Il est donc possible de créer tout type de widget et de les intégrer dans un tableau
de bord personnalisé :

Un utilisateur peut avoir accès  :

- à des applications externes 
    
- à des applications internes


Voir le guide du développeur openmairie
    
