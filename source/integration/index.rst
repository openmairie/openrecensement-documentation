.. _integration:


###########
Intégration
###########

Nous vous proposons dans ce chapître de décrire l'intégration d'openFoncier dans le SIG

- principes d'une application composite (mashup)

-  recupération des données de l'etat civil

- le tableau de bord parametrable (widget)



.. toctree::

    principes_integration.rst
    widget.rst
    

